document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent(event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown(content) {
	// Do your work here
	const composeLine = R.compose(composeBold, composeTag)
	return content.split(/\n/g).map(line => composeLine(line)).join('')
}

composeBold = line => {
	return line.replace(/\*(.*?)\*/g, text => setTag('b')(text.slice(1, -1))).replace(/\*/g,'')
}

composeTag = line => {
	if (line === '') {
		return '</br>'
	} else if (isHeader(line)) {
		return headerSelect(line)
	} else {
		return setTag('p')(line)
	}
}
isHeader = line => line.startsWith('#')
setTag = (tagName) => (line) => {
	return `<${tagName}>${line}</${tagName}>`
}
headerSelect = line => {
	const isSharp = char => char === '#'
	let count = R.takeWhile(isSharp, line).length
	return setTag(`h${count}`)(removeSharpFromLine(count, line))
}
removeSharpFromLine = (number, line) => {
	return line.substr(number + 1)
}

// String.prototype.replaceAt = function (index, char) {
// 	var a = this.split("");
// 	a[index] = char;
// 	return a.join("");
// }

// composeBold = line => {
// 	const openTag = line.indexOf('*')
// 	const closeTag = line.indexOf('*', openTag + 1)
// 	let tempLine = line
// 	if (closeTag !== -1) {
// 		tempLine = tempLine.replaceAt(openTag, '<b>')
// 		tempLine = tempLine.replaceAt(tempLine.indexOf('*'), '</b>')
// 		return composeBold(tempLine)
// 	} else {
// 		return tempLine.replaceAt(tempLine.indexOf('*'), '')
// 	}
// }